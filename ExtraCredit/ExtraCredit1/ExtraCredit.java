//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Extra Credit 1
//Date: April 10, 2015
//Purpose:Extra Credit and Practice
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Date;
import java.util.Scanner;

public class ExtraCredit
{
	public static void main(String[] args)
	{
        System.out.println("Dax Simitch Warke\nExtra Credit\n" + new Date() + "\n");

        System.out.println("Hello and welcome to the decimal to octal converter.");

        Scanner scan = new Scanner(System.in);

        int num;

        System.out.print("Please enter a number between 0 and 32767: ");
        num = scan.nextInt();

        if(32767 >= num || 0 <= num)
        {
            int num1 = (num - (num % 8)) / 8;
            int num2 = (num1 - (num1 % 8)) / 8;
            int num3 = (num2 - (num2 % 8)) / 8;
            int num4 = (num3 - (num3 % 8)) / 8;
            int num5 = (num4 - (num4 % 8)) / 8;

            int O5 = num % 8;
            int O4 = num1 % 8;
            int O3 = num2 % 8;
            int O2 = num3 % 8;
            int O1 = num4 % 8;

            System.out.println("The integer "+num+" is "+O1+O2+O3+O4+O5+" when converted to octal.");
        }
        if (32767 < num || 0 > num)
        {
            System.out.println("Unable to convert!");
        }
    }
}
