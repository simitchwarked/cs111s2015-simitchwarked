//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Lab 8
//March 26, 2015
//Purpose:if, else, and else if practice.
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Scanner;

public class SudokuChecker
{
    int [] puzzle = new int [16];
    public SudokuChecker()
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter your name: ");
        String name = scan.nextLine();
        System.out.println("");
        System.out.println("Hello "+name+" welcome to the Sudoku Checker!");
        System.out.println("");
        System.out.println("Here are the rules of the game.");
        System.out.println("");
        System.out.println("You must fill in the 4x4 grid using the numbers 1-4.");
        System.out.println("");
        System.out.println("Each of the 4 numbers being used may only appear once per row and column.");
        System.out.println("");
        System.out.println("Each number may also only appear once in each of the 2x2 quadrents.");
        System.out.println("");

        for(int i=0; i < puzzle.length; i++)
        {
            puzzle [i] = 0;
        }

          }

    public void getGrid()
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter rows 1-4 conseq: ");

        for(int i=0; i < puzzle.length; i++)
        {
            puzzle[i] = scan.nextInt();
        }
    }
    public void checkGrid()
    {
        int count = 1;
        for(int i=0; i<13; i+=4)
        {
            if(puzzle[i]+puzzle[i+1]+puzzle[i+2]+puzzle[i+3]==10)
            {
                if(i==0)
                System.out.println("Row 1 is correct");
                else if(i==4)
                System.out.println("Row 2 is correct");
                else if(i==8)
                System.out.println("Row 3 is correct");
                else if(i==12)
                System.out.println("Row 4 is correct");
            }
            else
            {
                System.out.println("This row is incorrect");
            }
        }
        for(int i=0; i<3; i++)
        {
            if(puzzle[i]+puzzle[i+4]+puzzle[i+4+4]+puzzle[i+4+4+4]==10)
            {
                System.out.println("The column "+(i+1)+" is correct");
            }
            else
            {
                System.out.println("The column "+(i+1)+" is incorrect");
            }
        }
        for(int i=0; i<11; i++)
        {
            if(i==0 || i==2 || i==8 || i==10)
            {
                if(puzzle[i]+puzzle[i+1]+puzzle[i+4]+puzzle[i+5]==10)
                {
                    System.out.println("This region is correct");
                }
                else
                {
                    System.out.println("This region is incorrect");
                }
            }    //all rows are correct
        }
    }
}
