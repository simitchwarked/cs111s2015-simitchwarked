//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Lab 8
//March 26, 2015
//Purpose:if, else, and else if practice.
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class SudokuMain
{
    public static void main (String[]args)
    {
        System.out.println("Dax Simitch Warke\nLab 8\n" + new Date() + "\n");

        Scanner scan = new Scanner(System.in);

        SudokuChecker foo = new SudokuChecker();
        foo.getGrid();
        foo.checkGrid();
    }
}

