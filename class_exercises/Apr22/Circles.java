//*****************************
//CS111 Spring 2015
//Class Exercise
//Dax Simitch Warke
//Purpose: Web program practice
//April 20, 2015
//*****************************
import java.applet.*;
import java.util.Random;
import java.awt.*;

public class Circles extends Applet
{
    int width = 700, height = 500;
    int N = 70;

    public void paint(Graphics g)
    {
        setBackground(Color.red);
        g.fillRect(0,0,width,height);
        int size = 5;
        g.setColor(Color.black);
        Random rand = new Random();
        int pos_x = 0;
        int pos_y = 0;

        for(int i=0; i < N; i++)
        {
            pos_x = rand.nextInt(width);
            pos_y = rand.nextInt(height);
            g.fillOval(pos_x, pos_y, size, size);
            size+=2;
        }
    }


}
