//==========================================
// Class Example
// Extension of April 1 program
//
// Purpose: This program demonstrates the usage of 
// the ArrayList and a while/for loops
//==========================================
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class ArrayListEx
{
    public static void main (String args[]) throws IOException 
    {
        ArrayList<String> allWords = new ArrayList<String>();
        ArrayList<String> allWordsBack = new ArrayList<String>();
        
        File file = new File("words.txt");
        Scanner input = new Scanner(file); 
        while (input.hasNext()) 
        { 
            String word = input.next(); 
            allWords.add(word); 
        } 
       
        // display all the words
        System.out.println(allWords);

        for (int i = allWords.size()-1; i > 0; i--) 
        { 
            allWordsBack.add(allWords.get(i));
        }
        System.out.println(allWordsBack);

        // replace 's' with capital 'S'
        for (int i = 0; i < allWords.size(); i++) 
        { 
            String word = allWords.get(i);
            if(word.contains("s"))
            {
                word = word.replace('s','S');
                allWords.set(i,word);
            }
        }
         System.out.println(allWords);


        // remove all plural words 
        for (int i = 0; i < allWords.size(); i++) 
        { 
            String word = allWords.get(i); 
            if (word.endsWith("s")) 
            { 
                allWords.remove(i); 
                i--; 
            } 
        }
        System.out.println(allWords); 
    }

}
