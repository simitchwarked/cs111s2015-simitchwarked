//****************************
// Dax Simitch Warke
// Class Exercise
// February 18, 2015
//
// Purpose: This program helps the user to create a random new identity
//****************************
import java.util.Scanner;
import java.util.Random;

public class NewIdentity
{

	public static void main(String args[])
	{
		String firstName, lastName, job;
		int age;
		float salary;

		// create an instance of the scanner
		Scanner scan = new Scanner(System.in);

		// create an instance of the random class
		Random rand = new Random();

		// get the user's input
		System.out.println("{Please enter a first name: ");
		firstName = scan.nextLine();
		System.out.println("Please enter a last name: ");
		lastName = scan.nextLine();
		System.out.println("Please enter your dream job: ");
		job = scan.nextLine();
		System.out.println("Please enter an integer: ");
		age = scan.nextInt();
		System.out.println("Please enter a floating point number: ");
		salary = scan.nextFloat();

		//randomly change user's input
		//get the length of the firstName
		int length = firstName.length();
		//get the position of the character in the firstName
		int position = rand.nextInt(length);
		System.out.println("Random position chosen is "+position);
		//pick a character at that position
		char randomLetter = firstName.charAt(position);
		System.out.println("Random letter at position "+position+" is "+randomLetter);
		
		//replace all of the selected characters with 'a'
		firstName = firstName.replace(randomLetter,'a');

		// append 'ov' to the lastName string
		lastName = lastName.concat("ov");
		lastName = lastName.replace(randomLetter,'c');
		lastName = lastName.replace(randomLetter,'i');
		lastName = lastName.replace(randomLetter,'r');

		//change the job to upper case
		job = job.toUpperCase();

		//assign a value for age
		age = rand.nextInt(age);

		//assign a value for salary
		salary = rand.nextFloat()*salary+1000;

		System.out.println("Your new name is "+firstName+" "+lastName+", and you are "+age+" years old. You work as a "+job+" making $"+salary+" a year.");
	}
}
