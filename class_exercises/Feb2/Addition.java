import java.util.Scanner;
// program uses class Scanner
public class Addition
{
	public static void main( String args[] )
	{
		int first; // first number to add
		int second; // second number to add
		int sum; // sum of first and second
		// create Scanner to obtain input from command window
		// System.in is the input complement to System.out
		Scanner input = new Scanner( System.in );
		System.out.print( "Enter first integer: " );
		first = input.nextInt(); // read from user
		System.out.print( "Enter second integer: " );
		second = input.nextInt();
		// read from user
		sum = first + second; // add numbers
		System.out.println( "Sum is " +sum );
	}
}
