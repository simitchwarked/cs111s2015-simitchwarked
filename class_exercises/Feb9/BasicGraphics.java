//=================================================
// Class Example using Graphics
// February 9, 2015
// Dax Simitch Warke
// 
// Purpose: Illustrate Graphics class methods
//=================================================
import javax.swing.*;
import java.awt.*;

public class BasicGraphics extends javax.swing.JApplet  {
	public void paint(Graphics g) {
        	/* Draw the square. */
          	g.setColor(Color.red);
          	g.fillRect(10, 20, 40, 40);
		g.fillOval(50, 20, 40, 40);
		g.drawLine(50, 50, 10, 5);
		g.drawArc(70, 70, 20, 20, 15, 45);
        }  // end paint()

	public static void main(String[] args)
    	{
        	JFrame window = new JFrame("Dax Simitch Warke ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new BasicGraphics());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);
        	
        	//window.pack();
    	}

} // end class BasicGraphics
