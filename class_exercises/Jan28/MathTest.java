//***************************
//Dax Simitch warke
//Class Exercise
//January 28, 2015
//
//Purpose: math calculations
//***************************

public class MathTest
{
	public static void main (String args[])
	{
		int x = 10;
		int y = 5;
		int result;

		result = x/y;
	
		System.out.println("Result is "+result);
		
		System.out.println(x/2);
		System.out.println(x/3);
		System.out.println(x/4);
		System.out.println(x/5);
		System.out.println(x/11);

		double z = 2.5;

		System.out.println(z*2.2);

		double result1 = z*2;

		System.out.println("Result1 is "+result1);
	}
}
