//=================================================
// Class Example using Math Class
// March 2, 2015
// CMPSC 111, Spring 2015
// Janyl Jumadinova
//
// Purpose:  Create a class GradeBookTest to instantiate GradeBook class
//=================================================
import java.util.Scanner;

public class GradeBookTest
{
    public static void main (String args[])
    {
        Scanner in = new Scanner (System.in);

        //System.out.println("Initial course name is "+myGradeBook.getCourseName());

        System.out.println("Please enter a course name");
        String courseName = in.nextLine();

        GradeBook myGradeBook1 = new GradeBook (courseName);

        System.out.println("Please enter a course name");
        String courseName1 = in.nextLine();
        GradeBook myGradeBook2 = new GradeBook (courseName1);

        System.out.println("Course name for 1 "+myGradeBook1.getCourseName());
        System.out.println("Course name for 2 "+myGradeBook2.getCourseName());

        myGradeBook2.setCourseName("GEO 101");
        System.out.println("A new course name is "+myGradeBook2.getCourseName());

        //System.out.println("Before!");
        //myGradeBook.displayMessage(CourseName);
        //System.out.println("After!");

    }
}
