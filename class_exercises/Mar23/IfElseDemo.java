/* CMPSC 111 Spring 2015
  Janyl Jumadinova
  Class Example
  March 23, 2015

  Purpose: To determine whether a grade a vowel
*/

import java.util.Scanner;
public class IfElseDemo
{
 	public static void main ( String args[] )
 	{
		Scanner input = new Scanner ( System.in );
 		System.out.print ( "Enter a character to test: " );
 	 	char character;	  	       		// new data type: char
 		character = input.next().charAt(0); 	// get character from input
 	 	if (character == 'a')     	      	// notice ' ' marks char
 		       	System.out.println ( character+" is a vowel." );
 	 	else if (character == 'e')
 			System.out.println ( character+" is a vowel." );
 	 	else if (character == 'i')
 			System.out.println ( character+" is a vowel." );
  		else if (character == 'o')
 			System.out.println ( character+" is a vowel." );
 		else if (character == 'u')
 			System.out.println ( character+" is a vowel." );
 		else
 			System.out.println ( character+" is not a vowel." );

        // if else - check for a specific letter
        if (character == 'z')
            System.out.println ( character+" is the last letter" );
        else
            System.out.println ( character+" is not the last letter" );
 	}
}

