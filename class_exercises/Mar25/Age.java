

//=================================================
// Class Example using if/else statements
// March 25, 2015
// CMPSC 111, Spring 2015
// Janyl Jumadinova
//
// Purpose: Age class has a constructor and two methods:
// updateAge to change the value of age to user specified value
// checkAge to check the age of the user
//=================================================/*
public class Age
{
    // instance variable
    private int age;

    // constructor
    public Age ( )
    {
        // initialize
        age = 0;

    }

    // updateAge method: change the value of 'age'
public void updateAge(int a)
    {
        age = a;
    }

    // checkAge method: check if the age is appropriate
    public boolean checkAge()
    {
        if(age < 12)
        {
            System.out.println("Sorry your too young");
            return false;
        }
        else if(age >=12 && age <18)
        {
            System.out.println("Please get a parent's consent");
            return true;
        }
        else
        {
            System.out.println("Hope you enjoy your gaming experience");
            return true;
        }
    }
}
