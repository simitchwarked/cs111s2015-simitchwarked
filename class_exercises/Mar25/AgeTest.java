//=================================================
// Class Example using if/else statements
// March 25, 2015
// CMPSC 111, Spring 2015
// Janyl Jumadinova
//
// Purpose: This class gets an age input from the user and
// then it connects to the class called 'Age' to check the age
//=================================================

import java.util.Scanner;
public class AgeTest
{
    public static void main ( String args[] )
    {
	    // create instances of Age
        Age a1 = new Age ( );
	    Scanner input = new Scanner ( System.in );
 	    System.out.print ( "Enter your age:" );
	    int age = input.nextInt();
        a1.updateAge(age);
	    boolean check = a1.checkAge();
        if(check!=true)
        {
            System.out.println("Sorry your too young to play");
        }
        else
        {
            System.out.println("Congratulations you are old enough to play");
        }

    }
}

