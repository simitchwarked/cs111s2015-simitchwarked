//==========================================
// Class Example
// March 30, 2014
//
// Purpose: This program demonstrates the usage of do while loop.
//==========================================

import java.util.Scanner;
import java.util.Random;

public class DoWhileLoopExample
{  
    public static void main ( String args[] )
    {
        
	Scanner scan = new Scanner ( System.in);
        Random rand = new Random();
	
	System.out.println(" Please enter five numbers.  ");
	int number;
	int i = 0;

	do
	{
		number = scan.nextInt();
		System.out.println(" Number "+(i+1)+" is :: " +number);	
		i++;
	} while(i<5);
	 
     }
} 

