//==========================================
// Class Example
// While loop
// March 30, 2014
//
// Purpose: This program demonstrates the usage of while loop.
// The program keeps count of user input and exits after user enters five numbers.
//==========================================

import java.util.Scanner;
import java.util.Random;

public class WhileLoopExample
{
    public static void main ( String args[] )
    {

        Scanner scan = new Scanner ( System.in);
        Random rand = new Random();

        System.out.println(" Please enter five numbers.  ");
        int number;
        int count = 0;

        // while i is less than 5, it will take in user's input
        while(count<5)
        {
            number = scan.nextInt();
            System.out.println(" Number "+(count+1)+" is :: " +number);
            count++;
            if(number == 5)
            {
                System.out.println("Can't have 5");
                continue;//break;
            }
        }
        System.out.println("Ok, we received your five numbers. Thank you!");

     }
}

