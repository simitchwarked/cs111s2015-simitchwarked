/*
Class Exercise - March 9, 2015
Dax S W
Purpose: create an account object with a constructor and three methods
*/

public class Account
{
    //instance variables
    private double balance;
    private int acNumber;
    //constructor
    public Account (double initBalance)
    {
        balance = initBalance;
    }

    public Account (int accountNo, double initBalance)
    {
        acNumber = accountNo;
        balance = initBalance;
    }

    //deposit method: adds amount to the balance
    public void deposit (double amount)
    {
        balance += amount;
    }

    //withdraw method: subtracts amount from the balance
    public void withdraw (double amount)
    {
        balance -= amount;
    }

    //getbalance method: returns the value of the balance
    public double getBalance()
    {
        return balance;
    }
}
