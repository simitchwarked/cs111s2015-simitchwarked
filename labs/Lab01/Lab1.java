//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dax Simitch Warke
// CMPSC 111 Spring 2015
// Lab # [1]
// Date: 01 22 2015
//
// Purpose: ... [understanding java]
// ***********************************

import java.util.Date;

public class Lab1
{
	public static void main ( String args[] )
	{
		System.out.println("Dax Simitch Warke "+ new Date());
		System.out.println("Lab 1");

		//Prints words of wisdom

		System.out.println("Advice from Majes Gosling, creator of Java");
		System.out.println("Don't be intimidated--give it a try!");

	}
}
