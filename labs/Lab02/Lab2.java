//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Lab 2
//Date: January 29, 2015
//Purpose: Preform Simple Calculation
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Date; // needed for printing today's date

public class Lab2
{
       // main method: program execution begins here
       public static void main(String[] args)
       {
	   // Label output with name and date:
	   System.out.println("Dax Simitch Warke\nLab 2\n" + new Date() + "\n");	   
           // Variables:
	   int x = 2;
           int y = 4;
           int z = 3;
           int w = 6;
	   int f = 8;
	   int k;
	   int p;
	   double j;
	   //Compute values:
	   k = z + y * x;
	   p = f * z - w;
	   j = Math.pow (z,x) * y;

	   System.out.println("Algebra problem 1: k = z + y * x: k = " + k);
	   
	   System.out.println("Algebra problem 2: p = f * z - w: p = " + p);
	   
	   System.out.println("Algebra problem 3: j = z^x * y: j = " + j);
	}
}
