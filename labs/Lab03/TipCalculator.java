//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Lab3 Tip Calculator
//February 9, 2015
//Purpose:Variable Practice
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Date;
import java.util.Scanner;

public class TipCalculator
{
	public static void main(String args [])
	{
		Scanner scan = new Scanner (System.in);

		System.out.println("Dax Simitch Warke\nLab 3\n" + new Date() + "\n");
		
		System.out.println("Hello!");
	
		System.out.print("Enter your bill amount: $");
		double Bill = scan.nextInt();

		System.out.print("Enter the percentage you would like to tip: ");	
		double Percentage = scan.nextInt();

		System.out.print("How many people will be spliting the bill today? ");
		double People = scan.nextInt();

		double D = 100;
		double Tip = Bill * Percentage / D;
		double Total = Tip + Bill;
		double TotalSplit = Total / People;

		System.out.println("Your Original total was: $"+Bill);
		System.out.println("Your tip amount is: $" +Tip);
		System.out.println("Your new total is: $"+Total);
		System.out.println("The amount per person is $"+TotalSplit);

		System.out.println("Have a nice day!");

	}
}

