//============================
// Dax Simitch Warke, Jared Nutter & Edward Tiemann
// CMPSC 111 Spring 2015
// Lab 4
// February 18, 2015
//
// Purpose: Team Program Building and Manipulation of DNA string
// Honor Code:This Original Thought
//============================
import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class DnaManipulation
{

    public static void main(String[] args)
    {
        System.out.println("Your Name\nLab #\n" + new Date() + "\n");

        Scanner scan = new Scanner(System.in);

        Random rand = new Random();

        String dnaString, dnaString2, dnaString3, dnaString4, dnaString5;
        String Beg, End, Beg2, End2, Beg3, End3;

        System.out.println("Please enter a string of 'A' 'T' 'C' 'G': ");
        dnaString = scan.nextLine();

        dnaString2 = dnaString.toUpperCase();
        dnaString2 = dnaString2.replace('A','a');
        dnaString2 = dnaString2.replace('T','A');
        dnaString2 = dnaString2.replace('a','T');
        dnaString2 = dnaString2.replace('C','c');
        dnaString2 = dnaString2.replace('G','C');
        dnaString2 = dnaString2.replace('c','G');

        System.out.println("Conjugate string is: "+dnaString2);

        dnaString3 = dnaString.toUpperCase();
        int Length = dnaString3.length();
        int Position = rand.nextInt(Length);
        char Let = "ACTG".charAt(rand.nextInt (4));
        Beg = dnaString3.substring(0,Position);
        End = dnaString3.substring(Position,Length);
        System.out.println("Randomly replaced String is: "+Beg+Let+End);

        dnaString4 = dnaString.toUpperCase();
        int Length2 = dnaString4.length();
        int Position2 = rand.nextInt(Length2);
        Beg2 = dnaString3.substring(0,(Position2));
        End2 = dnaString3.substring((Position2+1),Length);
        System.out.println("Removing position "+Position2+" gives "+Beg2+End2);

         dnaString5 = dnaString.toUpperCase();
        int Length3 = dnaString5.length();
        int Position3 = rand.nextInt(Length3);
        char Let2 = "ACTG".charAt(rand.nextInt (4));
        Beg3 = dnaString3.substring(0,(Position3));
        End3 = dnaString3.substring((Position3+1),Length);
        System.out.println("Removing position "+Position3+
        " and adding random letter "+Let2+ " gives: "+Beg3+Let2+End3);


    }
}
