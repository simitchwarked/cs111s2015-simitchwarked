//*****************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//February 26, 2015
//Purpose: To write a Java program that calculates compound interest using methods from Java’s Math and Format classes.
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*****************************
import java.util.Date;
import java.util.Scanner;
import java.text.DecimalFormat;
import java.text.NumberFormat;
public class CompoundCalculator
{
  	public static void main ( String args[] )
 	{
 		System.out.println("Dax Simitch Warke\nLab 5\n" + new Date() + "\n");

        double P, t, n, r, A1, A2;
 		NumberFormat fmt1 = NumberFormat.getPercentInstance();
        NumberFormat fmt2 = NumberFormat.getCurrencyInstance();
        Scanner scan = new Scanner (System.in);

        System.out.print( "Enter the initial amount borrowed or deposited: " );
        P = scan.nextDouble();

        System.out.print( "Enter the number of years the amount is borrowed or deposited for: " );
        t = scan.nextDouble();

        System.out.print( "Enter the number of times the interest is compounded per year: " );
        n = scan.nextDouble();

        r = 0.02 + (double)(Math.random()*0.13);
        System.out.println("Annual interest rate: "+fmt1.format(r));

        A1 = P * (Math.pow ((1 + (r / n)),n * t));

        A2 = P * (Math.pow (Math.E,r * t));

        System.out.println("Initial deposit or amount borrowed (P) = "+fmt2.format(P));

        System.out.println("Duration of deposits or borrowing (t) = "+t);

        System.out.println("Times per year the interest is compounded (n) = "+n);

        System.out.println("Annual interest rate (r) = "+fmt1.format(r));

        System.out.println("Periodic interest rate (A1=P(1+r/n)^nt) = "+fmt2.format(A1));
        System.out.println("Continous compounding rate (A2=Pe^rt) = "+fmt2.format(A2));
    }

}
