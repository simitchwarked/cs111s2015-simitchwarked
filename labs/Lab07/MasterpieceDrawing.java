//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Dax Simitch Warke
// CMPSC 111 Spring 2015
// Lab 7
// Date:03/05/2015
//
// Purpose: ... [describe the program]
//=================================================
import java.awt.*;
import javax.swing.*;

public class MasterpieceDrawing extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {

    // create an instance of Masterpiece class
    MasterpieceN PirateSkull;
    PirateSkull = new MasterpieceN(0,0,400,400);
    // call the methods in thinking Masterpiece class
    PirateSkull.drawBackground(page);
    PirateSkull.drawHead(page);
    PirateSkull.drawJaw(page);
    PirateSkull.drawEyesandnose(page);
    PirateSkull.drawMouth(page);
    PirateSkull.drawTeethup(page);
    PirateSkull.drawTeethdown(page);
    PirateSkull.drawEyepatch(page);

    // NOTE: you may have to pass 'page' in addition to any
    //       other arguments during your method call


    }

    // main method that runs the program
    public static void main(String[] args)
    {
        	JFrame window = new JFrame(" Pirate Skull ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new MasterpieceDrawing());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(400, 400);

    }
}
