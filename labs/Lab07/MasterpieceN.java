//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Your Name [Replace with your name]
// CMPSC 111 Spring 2015
// Lab 4
// Date: mmm dd yyyy [fill in the date]
//
// Purpose: ... [describe the program]
//=================================================
import java.awt.*;
import javax.swing.*;

public class MasterpieceN extends JApplet
{
    // instance variables
    private int height;   //canvas height
    private int width;    //canvas width
    private int x;        //cords
    private int y;        //cords
    // constructor
    public MasterpieceN(int t, int u, int v, int w)
    {
    x = t;
    y = u;
    height = v;
    width = w;
    }
    // methods
    // NOTE: your methods may need to take 'Graphics page' as a

    //draw:background
    public void drawBackground(Graphics page)
    {
        page.setColor(Color.black);
        page.fillRect(x,y,width,height);
    }
    //draw:head
    public void drawHead(Graphics page)
    {
        page.setColor(Color.white);
        page.fillOval(y+100, x+45, 200, 200);
        page.fillOval(y+100, x+60, 200, 200);
    }

    //draw:jaw
    public void drawJaw(Graphics page)
    {
        page.setColor(Color.white);
        page.fillRect(y+131, x+220, 140, 100);
    }

    //draw:eyes/nose
    public void drawEyesandnose(Graphics page)
    {
        page.setColor(Color.black);
        page.fillOval(y+135, x+118, 50, 50);
        page.fillOval(y+215, x+118, 50, 50);
        page.fillOval(y+180, x+188, 40, 40);
        page.setColor(Color.white);
        page.fillRect(y+127, x+108, 150, 20);
    }
    //draw:mouth
    public void drawMouth(Graphics page)
    {
        page.setColor(Color.black);
        page.fillRect(y+145, x+245, 110, 60);
    }

    //draw:teeth up
    public void drawTeethup(Graphics page)
    {
        page.setColor(Color.white);
        page.fillRect(151, 245, width-385, height-375);
        page.fillRect(172, 245, width-385, height-375);
        page.fillRect(193, 245, width-385, height-375);
        page.fillRect(214, 245, width-385, height-375);
        page.fillRect(235, 245, width-385, height-375);
    }

    //draw:teeth down
    public void drawTeethdown(Graphics page)
    {
        page.setColor(Color.white);
        page.fillRect(155, 280, width-385, height-375);
        page.fillRect(180, 280, width-385, height-375);
        page.fillRect(205, 280, width-385, height-375);
        page.fillRect(230, 280, width-385, height-375);
    }

    //draw:eye patch
    public void drawEyepatch(Graphics page)
    {
        page.setColor(Color.black);
        page.drawLine(215, 47, width-290, height-198);
    }

}
