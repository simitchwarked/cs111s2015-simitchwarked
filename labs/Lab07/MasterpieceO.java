//********************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Lab 4
//February 12, 2015
// Purpose: To create a Drawing.
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//********************************
import java.util.Date;
import javax.swing.*;
import java.awt.*;

public class Masterpiece extends JApplet  {
	public void paint(Graphics page) {

		System.out.println("Dax Simitch Warke\nLab 4\n" + new Date() + "\n");

		final int WIDTH = 400;
		final int HEIGHT = 400;

		page.setColor(Color.black);
		page.fillRect(0,0,WIDTH,HEIGHT);

		//head
		page.setColor(Color.white);
		page.fillOval(100, 45, 200, 200);
		page.fillOval(100, 60, 200, 200);

		//jaw
		page.setColor(Color.white);
		page.fillRect(131, 220, 140, 100);

		//eyes/nose
		page.setColor(Color.black);
		page.fillOval(135, 118, 50, 50);
		page.fillOval(215, 118, 50, 50);
		page.fillOval(180, 188, 40, 40);

		page.setColor(Color.white);
		page.fillRect(127, 108, 150, 20);

		//mouth
		page.setColor(Color.black);
		page.fillRect(145, 245, 110, 60);

		//teeth up

		int w = 15;
		int h = 25;
		int y1 = 245;
		int y2 = 280;

		page.setColor(Color.white);
		page.fillRect(151, y1, w, h);
		page.fillRect(172, y1, w, h);
		page.fillRect(193, y1, w, h);
		page.fillRect(214, y1, w, h);
		page.fillRect(235, y1, w, h);

		//teeth down
		page.setColor(Color.white);
		page.fillRect(155, y2, w, h);
		page.fillRect(180, y2, w, h);
		page.fillRect(205, y2, w, h);
		page.fillRect(230, y2, w, h);

		//eye patch
		page.setColor(Color.black);
		page.drawLine(215, 47, 110, 202);



	}
	// main method
	public static void main(String[] args)
    	{
        	JFrame window = new JFrame("Dax Simitch Warke ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new Masterpiece());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(400, 400);

        	//window.pack();
    	}

} // end class

