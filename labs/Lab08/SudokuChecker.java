//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Lab 8
//March 26, 2015
//Purpose:if, else, and else if practice.
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Scanner;

public class SudokuChecker
{

    String name;
    int w1, w2, w3, w4;
    int x1, x2, x3, x4;
    int y1, y2, y3, y4;
    int z1, z2, z3, z4;

    public SudokuChecker()
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter your name: ");
        name = scan.nextLine();
        System.out.println("");
        System.out.println("Hello "+name+" welcome to the Sudoku Checker!");
        System.out.println("");
        System.out.println("Here are the rules of the game.");
        System.out.println("");
        System.out.println("You must fill in the 4x4 grid using the numbers 1-4.");
        System.out.println("");
        System.out.println("Each of the 4 numbers being used may only appear once per row and column.");
        System.out.println("");
        System.out.println("Each number may also only appear once in each of the 2x2 quadrents.");
        System.out.println("");

        w1=0;
        w2=0;
        w3=0;
        w4=0;
        x1=0;
        x2=0;
        x3=0;
        x4=0;
        y1=0;
        y2=0;
        y3=0;
        y4=0;
        z1=0;
        z2=0;
        z3=0;
        z4=0;
    }

    public void getGrid()
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter row 1 with spaces: ");
        w1 = scan.nextInt();
        w2 = scan.nextInt();
        w3 = scan.nextInt();
        w4 = scan.nextInt();
        System.out.println("");

        System.out.print("Enter row 2 with spaces: ");
        x1 = scan.nextInt();
        x2 = scan.nextInt();
        x3 = scan.nextInt();
        x4 = scan.nextInt();
        System.out.println("");

        System.out.print("Enter row 3 with spaces: ");
        y1 = scan.nextInt();
        y2 = scan.nextInt();
        y3 = scan.nextInt();
        y4 = scan.nextInt();
        System.out.println("");

        System.out.print("Enter row 4 with spaces: ");
        z1 = scan.nextInt();
        z2 = scan.nextInt();
        z3 = scan.nextInt();
        z4 = scan.nextInt();
        System.out.println("");
    }

    public void checkGrid()
    {
        boolean check = true;

        if(w1+w2+w3+w4==10)
        {
            System.out.println("ROW-1:GOOD");
        }
        else
        {
            System.out.println("ROW-1:ERROR");
            check = false;
        }
        if(x1+x2+x3+x4==10)
        {
            System.out.println("ROW-2:GOOD");
        }
        else
        {
            System.out.println("ROW-2:ERROR");
            check = false;
        }
        if(y1+y2+y3+y4==10)
        {
            System.out.println("ROW-3:GOOD");
        }
        else
        {
            System.out.println("ROW-3:ERROR");
            check = false;
        }
        if(z1+z2+z3+z4==10)
        {
            System.out.println("ROW-4:GOOD");
        }
        else
        {
            System.out.println("ROW-4:ERROR");
            check = false;
        }
        System.out.println("");
        if(w1+x1+y1+z1==10)
        {
            System.out.println("COL-1:GOOD");
        }
        else
        {
            System.out.println("COL-1:ERROR");
            check = false;
        }
        if(w2+x2+y2+z2==10)
        {
            System.out.println("COL-2:GOOD");
        }
        else
        {
            System.out.println("COL-2:ERROR");
            check = false;
        }
        if(w3+x3+y3+z3==10)
        {
            System.out.println("COL-3:GOOD");
        }
        else
        {
            System.out.println("COL-3:ERROR");
            check = false;
        }
        if(w4+x4+y4+z4==10)
        {
            System.out.println("COL-4:GOOD");
        }
        else
        {
            System.out.println("COL-4:ERROR");
            check = false;
        }
        System.out.println("");
        if(w1+w2+x1+x2==10)
        {
            System.out.println("REG-1:GOOD");
        }
        else
        {
            System.out.println("REG-1:ERROR");
            check = false;
        }
        if(y1+y2+z1+z2==10)
        {
            System.out.println("REG-2:GOOD");
        }
        else
        {
            System.out.println("REG-2:ERROR");
            check = false;
        }
        if(w3+w4+x3+x4==10)
        {
            System.out.println("REG-3:GOOD");
        }
        else
        {
            System.out.println("REG-3:ERROR");
            check = false;
        }
        if(y3+y4+z3+z4==10)
        {
            System.out.println("REG-4:GOOD");
        }
        else
        {
            System.out.println("REG-4:ERROR");
            check = false;
        }
        System.out.println("");
        if (check)
        {
            System.out.println("SUDO:VALID");
        }
        else
        {
            System.out.println("SUDO:INVALID");
        }
    }
}
