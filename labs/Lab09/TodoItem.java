//class start
public class TodoItem {

    //instance variables
    private int id;
    private static int nextId = 0;
    private String priority;
    private String category;
    private String task;
    private boolean done;

    //constructor
    public TodoItem(String p, String c, String t)
    {
        id = nextId;
        nextId++;
        priority = p;
        category = c;
        task = t;
        done = false;
    }
    //Calls getId method
    public int getId()
    {
        return id;
    }
    //calls getPriority method
    public String getPriority()
    {
        return priority;
    }
    //calls getcategory method
    public String getCategory()
    {
        return category;
    }
    //calls gettask method
    public String getTask()
    {
        return task;
    }
    //determines if its the end of task
    public void markDone()
    {
        done = true;
    }
    //ends task
    public boolean isDone()
    {
        return done;
    }
    //asks for a new task or if your done
    public String toString()
    {
        return new String(id + ", " + priority + ", " + category + ", " + task + ", done? " + done);
    }

}
