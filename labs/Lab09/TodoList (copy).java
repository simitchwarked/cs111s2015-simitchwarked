//importing various utilities from the Java library.
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList  //Start class
{
    //Instance variables
    private ArrayList<TodoItem> todoItems;
    private static final String TODOFILE = "todo.txt";
    //Constructor
    public TodoList()
    {
        todoItems = new ArrayList<TodoItem>();
    }
    //Method: puts items in todoitems list
    public void addTodoItem(TodoItem todoItem)
    {
        todoItems.add(todoItem);
    }
    //Method: get todo items from file
    public Iterator getTodoItems()
    {
        return todoItems.iterator();
    }
    //Method: checking to see if the file is still there and stopping if it isn't
    public void readTodoItemsFromFile() throws IOException
    {
        Scanner fileScanner = new Scanner(new File(TODOFILE));
        // requires loop until all tasks in the list are finished
        while(fileScanner.hasNext())
        {
            String todoItemLine = fileScanner.nextLine();
            Scanner todoScanner = new Scanner(todoItemLine);
            todoScanner.useDelimiter(",");
            String priority, category, task;
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task);
            todoItems.add(todoItem);
        }
    }
    //Method: gives task id and marks it as done
    public void markTaskAsDone(int toMarkId)
    {
        Iterator iterator = todoItems.iterator();

        while(iterator.hasNext())
        {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId)
            {
                todoItem.markDone();
            }
        }
    }
    //Method: Determines task priority
    public Iterator findTasksOfPriority(String requestedPriority)
    {
        Iterator iterator = todoItems.iterator();
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
            while(iterator.hasNext())
            {
            	TodoItem todoItem = (TodoItem)iterator.next();
            	if(todoItem.getPriority().equals(requestedPriority))
            	{
                	priorityList.add(todoItem);
            	}
            }
        return priorityList.iterator();
    }
    //Method: determines task category
    public Iterator findTasksOfCategory(String requestedCategory)
    {
        Iterator iterator = todoItems.iterator();
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
            while(iterator.hasNext())
            {
            	TodoItem todoItem = (TodoItem)iterator.next();
            	if(todoItem.getCategory().equals(requestedCategory))
            	{
                	categoryList.add(todoItem);
            	}
            }
        return categoryList.iterator();
    }
    //Method: ends todolist actions
    public String toString()
    {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext())
        {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext())
            {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}

