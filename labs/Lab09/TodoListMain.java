//*************************
//Dax Simitch Warke, Ian Edwards,Omar Kalash
//CMPSC 111 Spring 2015
//Lab 9
//Date April 2, 2015
//Purpose: Showing Understanding of code
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
    //imports java libraries
import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;
//starts class
public class TodoListMain
{
    //main method begin
    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, done, list, quit");
    //creates instance of scanner and todolist objects
        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();
        //reads and responds to user input
        while(scanner.hasNext()) {
            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            else if(command.equals("done")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsDone(chosenId);
            }
            else if(command.equals("priority-search"))
            {
                System.out.println("What is the priority of the task?");
                String p = scanner.next();
                Iterator priorityList = todoList.findTasksOfPriority(p);
                while (priorityList.hasNext())
                {
                	System.out.println(priorityList.next());
                }
            }
            else if(command.equals("category-search"))
            {
                System.out.println("What is the category of the task?");
                String c = scanner.next();
                Iterator categoryList = todoList.findTasksOfCategory(c);
                while (categoryList.hasNext())
                {
                	System.out.println(categoryList.next());
                }
            }
            else if(command.equals("quit")) {
                break; //ends program
            }
        }

    }

}
