//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Lab 10
//Date:April 9 2015
//Purpose:Practice and Understanding
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Date;
import java.util.Scanner;

public class NumerologyReport
{
    static String[] fortune = new String[9];
    public static void main(String[] args)
    {
        //fortunes
        fortune[0] = "YOU WILL COME IN TO RICHES IN THE NEAR FUTURE.";
        fortune[1] = "YOU WILL MEET A BEAUTIFUL WOMAN IN THE NEAR FUTURE";
        fortune[2] = "YOUR JOB IS WHAT YOU WERE INTENDED TO DO";
        fortune[3] = "YOUR CHILDREN WILL GROW UP TO BE SKETCHY";
        fortune[4] = "YOU WILL GET A DOG IN THE NEXT YEAR";
        fortune[5] = "YOU WILL FIND YOUR SELF IN A PLACE OF HARMONY";
        fortune[6] = "YOU WILL BE FIRED NEXT WEEK";
        fortune[7] = "YOU WILL NEVER BE SUCCESSFULL";
        fortune[8] = "A SPECIAL SOMEONE IS WAITING FOR YOU IN THE NEAR FUTURE";

        System.out.println("Dax Simitch Warke\nLab 10\n" + new Date() + "\n");

        System.out.println("Hello and welcome to the numerology report program.");
        Scanner scanner = new Scanner(System.in);
        //variables
        boolean correct = false;
        int month = 0;
        int day = 0;
        int year = 0;
        // year month day validation
        while(!correct)
        {
            System.out.println("Please enter your birthday in this fashion, M / D / YYYY:");
            month = scanner.nextInt();
            scanner.next().charAt(0);
            day = scanner.nextInt();
            scanner.next().charAt(0);
            year = scanner.nextInt();


            if((1880 <= year || year <= 2280) && (1 <= month || month <= 12))
            {
                if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
                {
                    if(1 <= day || day <= 31)
                    {
                        correct = true;
                    }
                    else
                    {
                        System.out.println("Invalid day");
                        correct = false;
                    }
                }
                else if(month == 4 || month == 6 || month == 9 || month == 11)
                {
                    if(1 <= day || day <= 30)
                    {
                        correct = true;
                    }
                    else
                    {
                        System.out.println("Invalid day");
                        correct = false;
                    }

                }
                else if(month == 2 && (year%100!=0 && year%4==0) || (year%100==0 && year%400==0))
                {
                    if(1 <= day || day <= 29)
                    {
                        correct = true;
                    }
                    else
                    {
                        System.out.println("Invalid day");
                        correct = false;
                    }
                }

                else
                {
                    if(1 <= day || day <= 28)
                    {
                        correct = true;
                    }
                    else
                    {
                        System.out.println("Invalid day");
                        correct = false;
                    }

                }

            }
            if(1 > month || 12 < month)
            {
                System.out.println("Invalid month");
                correct = false;
            }
            else if(1880 > year || 2280 < year)
            {
                System.out.println("Invalid year");
                correct = false;
            }
        }
        // fortune number determination
        int crunch = month+day+year;
        int sum = crunch;
        while(sum > 9)
        {
            sum = 0;
            do
            {
                sum += crunch%10;

                crunch /= 10;
            }
            while(crunch > 0);
            crunch = sum;
        }
        int numerologyCrunch = sum;
        System.out.println(fortune[numerologyCrunch -1]);
    }
}
