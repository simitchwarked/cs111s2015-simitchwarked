//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dax Simitch Warke
// CMPSC 111 Spring 2015
// Practical 1 
// Date: 01 22 2015
//
// Purpose: Setting up Bitbucket
// ***********************************

public class Welcome
{
	public static void main ( String args[] )
	{
		System.out.print("Welcome to your first programming exercise ");
		System.out.println("in CMPSC "+(100+11)+"!\n");
	}
}
