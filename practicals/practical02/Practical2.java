//*************************
//Dax Simitch Warke
//Practical 2
//29 January 2015
//
//Prints my name
//No Honor Code Violations
//*************************
import java.util.Date;
public class Practical2
{
  public static void main(String[] args)
  {
	System.out.println("Dax, CMPSC 111\n" + new Date() + "\n");
	System.out.println("---         --         \\    /");
	System.out.println("|   \\      /  \\         \\  /");
	System.out.println("|    \\    /    \\         \\/");
	System.out.println("|     |  /------\\        /\\");
	System.out.println("|    /  /        \\      /  \\");
	System.out.println("|   /  /          \\    /    \\");
	System.out.println("---   /            \\  /      \\");
   }
}
