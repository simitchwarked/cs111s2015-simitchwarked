
//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Practical03
//Date: Feb 5 2015
//Purpose:Variable Practice
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Scanner;

public class Madlibs
{
	public static void main(String args [])
	{
	    String Adjective;
	    String PluralNoun;
	    String Verb; 
	    String Noun;
	    String A = "The"; 
	    String B = " adventurer entered the cursed tomb.";
	    String C = "Upon his entry to the old tomb he saw old";
	    String D = " on the walls.";
	    String E = "After hours of";
	    String F = " the adventurer found the main room.";
	    String G = "In the main chamber the adventurer found ten chests filled with";
	    String H = " and became rich forever.";
	    
	    Scanner scan = new Scanner (System.in);

	    System.out.println("Dax Simitch Warke\nPractical03\n");

	    System.out.print("The _______ adventurer entered the cursed tomb. Enter an Adjective");
	    Adjective = scan.nextLine();
	
	    System.out.print("Upon his entry to the old tomb he saw old ________ on the walls. Enter a Plural Noun");
	    PluralNoun = scan.nextLine();

            System.out.print("After hours of _______ the adventurer found the main room. Enter a Verb");
	    Verb = scan.nextLine();

            System.out.print("In the main chamber the adventurer found ten chests filled with _______ and became rich forever. Enter a Noun");
	    Noun = scan.nextLine();

	    System.out.println("The Complete Story: "+(A+Adjective+B+C+PluralNoun+D+E+Verb+F+G+Noun+H));

	}
}





