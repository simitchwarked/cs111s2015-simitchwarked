/************************************
 Honor Code: The work I am submitting is
 a result of my own thinking and efforts.
 Dax Simitch Warke
 CMPSC 111
 26 February 2015
 Practical 5

 Basic Input with a dialog box
************************************/

import javax.swing.JOptionPane;

public class Practical5
{
    public static void main ( String[] args )
    {

		// display a dialog with a message
		JOptionPane.showMessageDialog( null, "Welcome. I have some questions to ask you!");

		// prompt user to enter name
		String LastName = JOptionPane.showInputDialog(" What is your last name?");

        String Animal = JOptionPane.showInputDialog("What is your favorite animal?");

		//create a message
		String message = String.format ("My favorite baseball team has a player called, %s, on it! ", LastName);

        String message2 = String.format ("Cool!, %s,'s are known to be very intelligent. ", Animal);

		//display the message to welcome the user by name
		JOptionPane.showMessageDialog(null, message);
        JOptionPane.showMessageDialog(null, message2);
    } //end main
}  //end class Practical5
