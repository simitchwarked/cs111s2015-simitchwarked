//====================================
// CMPSC 111
// Practical 6
// 5--6 March 2015
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical6
{
    public static void main(String[] args)
    {
        System.out.println("Janyl Jumadinova\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // an octopus
        Octopus bob;            // a second octopus
        Utensil spat;           // a kitchen utensil
        Utensil brd;            // a second utensil

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        brd = new Utensil("Bread Knife");
        brd.setColor("purple");
        brd.setCost(14.00);

        ocky = new Octopus("Ocky",10);    // create and name the octopus
        //ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        bob = new Octopus ("Bob",13);
        bob.setWeight(90);
        bob.setUtensil(brd);

        //ocky
        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor()+"\n");

        //bob
        System.out.println(bob.getName() + " weighs " +bob.getWeight()
            + " pounds\n" + "and is " + bob.getAge()
            + " years old. His favorite utensil is a "
            + bob.getUtensil());

        System.out.println(bob.getName() + "'s " + bob.getUtensil() + " costs $"
            + bob.getUtensil().getCost());
        System.out.println("Utensil's color: " + brd.getColor());

        // Use methods to change some values:

        bob.setAge(26);
        bob.setWeight(180);
        brd.setCost(19.99);
        brd.setColor("green");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(bob.getName() + "'s new age: " + bob.getAge());
        System.out.println(bob.getName() + "'s new weight: " + bob.getWeight());
        System.out.println("Utensil's new cost: $" + brd.getCost());
        System.out.println("Utensil's new color: " + brd.getColor());

    }
}
