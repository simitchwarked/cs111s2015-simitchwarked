//*****************************
// CMPSC 111
// Practical 7
// March 26, 2015
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;
public class Practical7Main
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.println("Hello and welcome to the year checker program!");
        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

        boolean LeapYear = activities.isLeapYear();
        boolean CicadaYear = activities.isCicadaYear();
        boolean SunspotYear = activities.isSunspotYear();

        System.out.println("Thank you for using this program.");
    }
}
