//*****************************
// CMPSC 111
// Practical 7
// March 26, 2015
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

import java.util.Scanner;

public class YearChecker
{
    //Create instance variables
    int year;

    //Create a constructor
    public YearChecker(int y)
    {
        year = y;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear()
    {
        if(year%100!=0 && year%4==0)
        {
            System.out.println("It's a leap year!");
            return true;
        }
        else if(year%100==0 && year%400==0)
        {
            System.out.println("It's a leap year!");
            return true;
        }
        else if(year%100==0 && year%400!=0)
        {
            System.out.println("It's not a leap year.");
            return false;
        }
        else
        {
            System.out.println("It's not a leap year.");
            return false;
        }
    }

    // a method that checks if the user's input year is a cicada year
    public boolean isCicadaYear()
    {
        if((year-2013)%17==0)
        {
            System.out.println("It's a year for the 17-year cicadas!");
            return true;
        }
        else if((2013-year)%17==0)
        {
            System.out.println("It's a year for the 17-year cicadas!");
            return true;
        }
        else
        {
            System.out.println("It's not a year for the 17-year cicadas.");
            return false;
        }
    }

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear()
    {
        if((year-2013)%11==0)
        {
            System.out.println("It's a peak year for sunspot activity!");
            return true;
        }
        else if((2013-year)%11==0)
        {
            System.out.println("It's a peak year for sunspot activity!");
            return true;
        }
        else
        {
            System.out.println("It's not a peak year for sunspot activity.");
            return false;
        }
    }
}
