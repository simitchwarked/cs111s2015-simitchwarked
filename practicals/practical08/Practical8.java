//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Practical 8
//Date: April 2, 2015
//Purpose:if/else while loop statement practice
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class Practical8
{
    public static void main(String[] args)
    {

        System.out.println("Dax Simitch Warke\nPractical 8\n" + new Date() + "\n");

        Scanner scan = new Scanner ( System.in);
        Random rand = new Random();

        int Variable = rand.nextInt(100)+1;
        int Guess;
        int numTries = 0;

        System.out.println("A random number has been generated.");
        System.out.println("How many guess's will it take you to find out the number?");
        System.out.print("Enter your first guess here: ");
        Guess = scan.nextInt();
        numTries++;

        while(Guess != Variable)
        {
            if (Guess < Variable)
            {
                System.out.println("Your guess was too low!");
                numTries++;
            }
            if (Guess > Variable)
            {
                System.out.println("Your guess was too high!");
                numTries++;
            }
            if (Guess == Variable)
            {
                numTries++;
                break;
            }
            System.out.print("Please guess again: ");
            Guess = scan.nextInt();
        }
        System.out.println("That is correct!");
        System.out.println("Total Tries: "+numTries);
    }
}
