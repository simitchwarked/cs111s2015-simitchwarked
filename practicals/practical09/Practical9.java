//*************************
//Dax Simitch Warke
//CMPSC 111 Spring 2015
//Practical 9
//Date: April 16 2015
//Purpose:Practice with arrays
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//*************************
import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class Practical9
{
    public static void main(String[] args)
    {
        System.out.println("Dax Simitch Warke\nPractical 9\n" + new Date() + "\n");

        System.out.println("Welcome to the dice machine.");

        Scanner scan = new Scanner(System.in);
        Random rand = new Random();

        int [] DiceRoll = new int [1000];
        int Dice1, Dice2;
        int max, index;

        for(int i=0; i<1000; i++)
        {
            Dice1 = rand.nextInt(6)+1;
            Dice2 = rand.nextInt(6)+1;
            DiceRoll[i] = Dice1 + Dice2;

            System.out.println("Roll "+(i+1)+" was "+Dice1+" and "+Dice2);
        }

        index = 0;
        max = DiceRoll[0];

        for(int i=0; i<1000; i++)
        {
            if(DiceRoll[i]>max)
            {
                max = DiceRoll[i];
                index = i;
            }
        }

        System.out.println("The maximum value is "+max);
        System.out.println("This occurred at index "+index);
    }
}
