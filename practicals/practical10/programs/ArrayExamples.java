//*********************************************************************************
// CMPSC 111 Spring 2015
// Practical 10
//
// Purpose: Program demonstrating various array manipulations
//*********************************************************************************
public class ArrayExamples
{
    public static void main(String[] args)
    {

        int findMin = 0;
        int[] list = {1, 2, 3, 4, 1, 2, 3};
        int minIndex = findMin;
        System.out.println("Minimum value in the list is "+list[minIndex]);
        showList(list);

        list = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        minIndex = findMin;
        System.out.println("Minimum value in the list is "+list[minIndex]);
        showList(list);

        list = new int[] {11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2};
        minIndex = findMin;
        System.out.println("Minimum value in the list is "+list[minIndex]);
        showList(list);

        // TO DO: Create a new array, print the minimum value from that array and then print all of the elements in the array
        list = new int[] {13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4};
        minIndex = findMin;
        System.out.println("Minimum value in the list is "+list[minIndex]);
        showList(list);
    }

    // return index of minimum element of array
    public static int findMin(int[] list)
    {
        int indexOfMin = 0;
        for(int i = 1; i < list.length; i++)
        {
            if(list[i] < list[indexOfMin])
            {
                indexOfMin = i;
            }
        }
        return indexOfMin;
    }

    // print out the elements in the list
    public static void showList(int[] list)
    {
        for(int i = 0; i < list.length; i++)
            System.out.print( list[i] + " " );
        System.out.println();
    }

}

