//*********************************************************************************
// CMPSC 111 Spring 2015
// Practical 10
//
// Purpose: Program demonstrating  illustrates how to call static methods a class
// from a method in the same class
//*********************************************************************************

public class CallingMethodsInSameClass
{
    public static void main(String[] args)
    {
        printOne();
        printOne();
        printTwo();
        printThree();
        // TO DO: add printThree() method call
    }

    public static void printOne()
    {
        System.out.println("Nothing is impossible, the word itself says, I'm possible!");
    }

    public static void printTwo()
    {
        printOne();
        printOne();
    }

    // TO DO: write a new method called printThree() that calls method printTwo() one time
    public static void printThree()
    {
        printOne();
        printOne();
        printOne();
    }

}
