//*********************************************************************************
// CMPSC 111 Spring 2015
// Practical 10
//
// Purpose: Program demonstrating usage of while and for loops with the Iterator class
//*********************************************************************************
import java.util.*;

public final class LoopStyles
{

    public static void main(String[] args)
    {
        ArrayList<String> flavours = new ArrayList<String>();
        flavours.add("chocolate");
        flavours.add("strawberry");
        flavours.add("vanilla");

        // TO DO: use an appropriate ArrayList method to change the "strawberry" flavour to some other flavour
        flavours.set(1, "cherry");


        useWhileLoop(flavours);

        useForLoop(flavours);
    }

    private static void useWhileLoop(ArrayList<String> flavours)
    {
        Iterator<String> flavoursIter = flavours.iterator();
        while (flavoursIter.hasNext())
        {
            System.out.println(flavoursIter.next());
        }
    }

    /**
     * Note that this for-loop does not use an integer index.
     */
    private static void useForLoop(ArrayList<String> flavours)
    {
        for (Iterator<String> flavoursIter = flavours.iterator(); flavoursIter.hasNext();)
        {
            System.out.println(flavoursIter.next());
        }
    }
}

