//*********************************************************************************
// CMPSC 111 Spring 2015
// Practical 10
//
// Purpose: Program demonstrating reading input from the terminal
//*********************************************************************************
import java.util.Scanner;

public class ScannerAndKeyboard
{
    public static void main(String[] args)
    {

        Scanner s = new Scanner(System.in);
        System.out.print( "Enter your name: "  );
        String name = s.nextLine();

        String str = name;
        int n = str.length();
        char last = str.charAt(n-1);
        char first = str.charAt(0);


        System.out.println( "Hello " + name + "!" );

        System.out.println("The first letter is "+first+" and the last letter is "+last+ ".");
    // TO DO: Using an appropriate method, obtain the first and the last letters of the user's name and print both of them
    }
}
